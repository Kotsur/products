package com.example.product.web;

import com.example.product.entity.Product;
import com.example.product.payload.request.ProductRequest;
import com.example.product.service.ProductService;
import com.example.product.validations.ResponseErrorValidation;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
public class ProductController {
    private final ProductService productService;
    private final ResponseErrorValidation responseErrorValidation;

    /**
     * Constructs a new ProductController with the specified ProductService and ResponseErrorValidation.
     *
     * @param productService          The ProductService used to perform product-related operations.
     * @param responseErrorValidation The ResponseErrorValidation used for handling validation errors.
     */
    public ProductController(ProductService productService, ResponseErrorValidation responseErrorValidation) {
        this.productService = productService;
        this.responseErrorValidation = responseErrorValidation;
    }

    /**
     * Retrieves all products.
     *
     * @return ResponseEntity containing a list of Product objects representing all the products.
     */
    @GetMapping("/products")
    public ResponseEntity<List<Product>> getAllProducts() {
        return productService.getAllProducts();
    }

    /**
     * Retrieves a product by ID.
     *
     * @param idProducts The ID of the product to retrieve.
     * @return ResponseEntity containing a ProductResponse object representing the retrieved product.
     */
    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProducts(@PathVariable("id") Long idProducts) {
        return productService.getProductById(idProducts);
    }

    /**
     * Creates a new product.
     *
     * @param productRequest The ProductRequest object containing the details of the product to be created.
     * @param bindingResult  The BindingResult object that holds the validation result.
     * @return ResponseEntity containing a ProductResponse object representing the created product, or error response if validation fails.
     */
    @PostMapping("/products")
    public ResponseEntity<Object> createProducts(@Valid @RequestBody ProductRequest productRequest, BindingResult bindingResult) {
        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) {
            return errors;
        }
        return productService.createProduct(productRequest);
    }

    /**
     * Updates an existing product.
     *
     * @param idProducts      The ID of the product to be updated.
     * @param productRequest  The ProductRequest object containing the updated details of the product.
     * @param bindingResult   The BindingResult object that holds the validation result.
     * @return ResponseEntity containing a ProductResponse object representing the updated product, or error response if validation fails.
     */

    @PutMapping("/products/{id}")
    public ResponseEntity<Object> updateProducts(@PathVariable("id") Long idProducts, @Valid @RequestBody ProductRequest productRequest,  BindingResult bindingResult) {
        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) {
            return errors;
        }
        return productService.updateProduct(idProducts, productRequest);
    }
    /**
     * Deletes a product.
     *
     * @param idProducts The ID of the product to be deleted.
     * @return ResponseEntity containing a DeleteProductResponse object indicating that the product has been deleted.
     */
    @DeleteMapping("/products/{id}")
    public ResponseEntity<Object> deleteProducts(@PathVariable("id") Long idProducts) {
        return productService.deleteProduct(idProducts);
    }
}
