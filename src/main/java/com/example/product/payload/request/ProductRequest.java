package com.example.product.payload.request;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
public class ProductRequest {
    @NotEmpty(message = "Name cannot be empty")
    private String name;

    @Size(min = 1, max = 255, message = "Description length must be between 1 and 255 characters")
    private String description;
    @DecimalMin(value = "0.0")
    @NotNull(message = "price cannot be empty")
    private Double price;
}
