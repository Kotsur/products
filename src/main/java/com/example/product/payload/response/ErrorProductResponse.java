package com.example.product.payload.response;

import com.example.product.exeption.ProductException;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;
@Data
public class ErrorProductResponse {
    private Map<String, String> errorMap = new HashMap<>();
    private ProductException productException;

    public ErrorProductResponse(ProductException productException) {
        this.productException = productException;
        errorMap.put("error", productException.getMessage());
    }
}
