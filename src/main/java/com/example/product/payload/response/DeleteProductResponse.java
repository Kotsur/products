package com.example.product.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DeleteProductResponse {
    private Long id;
    private String name;
    private String message;

}
