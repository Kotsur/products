package com.example.product.service;

import com.example.product.entity.Product;
import com.example.product.exeption.ProductException;
import com.example.product.payload.request.ProductRequest;
import com.example.product.payload.response.DeleteProductResponse;
import com.example.product.payload.response.ErrorProductResponse;
import com.example.product.payload.response.ProductResponse;
import com.example.product.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * The implementation of ProductService interface.
 */
@Service
public class ProductServiceImp implements ProductService {
    public static final Logger LOG = LoggerFactory.getLogger(ProductServiceImp.class);
    @Autowired
    private ProductRepository productRepository;

    /**
     * Retrieves all products from the database.
     *
     * @return ResponseEntity containing a list of Product objects representing all the products.
     */
    @Override
    public ResponseEntity<List<Product>> getAllProducts() {
        List<Product> productList = productRepository.findAll();
        return ResponseEntity.ok(productList);
    }
    /**
     * Retrieves a product by its ID.
     *
     * @param idProduct The ID of the product to retrieve.
     * @return ResponseEntity containing a ProductResponse object representing the retrieved product.
     * @throws ProductException Thrown when the product with the given ID is not found.
     */
    @Override
    public ResponseEntity<Object> getProductById(Long idProduct) {
        try {
            Product product = productRepository.findById(idProduct).orElseThrow(() -> new ProductException("Product not found"));
            return ResponseEntity.ok(
                    new ProductResponse(
                            product.getId(),
                            product.getName(),
                            product.getDescription(),
                            product.getPrice())
            );
        } catch (ProductException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(new ErrorProductResponse(e).getErrorMap());
        }
    }

    /**
     * Creates a new product.
     *
     * @param productRequest The ProductRequest object containing the details of the product to be created.
     * @return ResponseEntity containing a ProductResponse object representing the created product.
     * @throws ProductException Thrown when there is an error saving the product.
     */
    @Override
    public ResponseEntity<Object> createProduct(ProductRequest productRequest) {
        Product product = new Product(productRequest.getName(), productRequest.getDescription(), productRequest.getPrice());
        try {
            product = productRepository.save(product);
            LOG.info("Saving product " + product.getName());
            return ResponseEntity.ok(
                    new ProductResponse(
                            product.getId(),
                            product.getName(),
                            product.getDescription(),
                            product.getPrice())
            );
        } catch (Exception e) {
            LOG.error("Error save product. " + e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new ErrorProductResponse(new ProductException("Error save product")).getErrorMap());


        }
    }
    /**
     * Updates an existing product.
     *
     * @param idProduct       The ID of the product to be updated.
     * @param productRequest The ProductRequest object containing the updated details of the product.
     * @return ResponseEntity containing a ProductResponse object representing the updated product.
     * @throws ProductException Thrown when the product with the given ID is not found or there is an error updating the product.
     */
    @Override
    public ResponseEntity<Object> updateProduct(Long idProduct, ProductRequest productRequest) {
        try {
            Product product = productRepository.findById(idProduct).orElseThrow(() -> new ProductException("Product not found"));
            product.setName(productRequest.getName());
            product.setDescription(productRequest.getDescription());
            product.setPrice(productRequest.getPrice());
            product = productRepository.save(product);
            LOG.info("Update product " + product.getName());
            return ResponseEntity.ok(
                    new ProductResponse(
                            product.getId(),
                            product.getName(),
                            product.getDescription(),
                            product.getPrice())
            );
        } catch (ProductException e) {
            LOG.error("Error Update product. " + e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new ErrorProductResponse(e).getErrorMap());
        }
    }
    /**
     * Deletes a product.
     *
     * @param idProduct The ID of the product to be deleted.
     * @return ResponseEntity containing a DeleteProductResponse object indicating that the product has been deleted.
     * @throws ProductException Thrown when the product with the given ID is not found or there is an error deleting the product.
     */
    @Override
    public ResponseEntity<Object> deleteProduct(Long idProduct) {
        try {
            Product product = productRepository.findById(idProduct).orElseThrow(() -> new ProductException("Product not found"));
            DeleteProductResponse deleteProductResponse = new DeleteProductResponse(product.getId(), product.getName(), "product delete");
            productRepository.delete(product);
            LOG.info("delete product id: " + product.getId()+" name:"+product.getName());
            return ResponseEntity.ok(deleteProductResponse);
        }catch (ProductException e){
            LOG.error("Error delete product. " + e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new ErrorProductResponse(e).getErrorMap());
        }
    }
}
