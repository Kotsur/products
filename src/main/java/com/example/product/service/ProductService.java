package com.example.product.service;

import com.example.product.entity.Product;
import com.example.product.payload.request.ProductRequest;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ProductService {
    ResponseEntity<List<Product>> getAllProducts();
    ResponseEntity<Object> getProductById(Long idProduct);
    ResponseEntity<Object> createProduct(ProductRequest productRequest);
    ResponseEntity<Object> updateProduct(Long idProduct, ProductRequest productRequest);
    ResponseEntity<Object> deleteProduct(Long idProduct);

}
