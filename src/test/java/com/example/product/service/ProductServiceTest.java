package com.example.product.service;

import com.example.product.entity.Product;
import com.example.product.exeption.ProductException;
import com.example.product.payload.request.ProductRequest;
import com.example.product.payload.response.DeleteProductResponse;
import com.example.product.payload.response.ErrorProductResponse;
import com.example.product.payload.response.ProductResponse;
import com.example.product.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class ProductServiceTest {
    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductServiceImp productService;

    @Test
    public void getProductById_ExistingId_ReturnsProductResponse() {
        Long productId = 1L;
        Product product = new Product(productId, "Product 1", "Description 1", 10.0);
        when(productRepository.findById(productId)).thenReturn(Optional.of(product));

        ResponseEntity<Object> response = productService.getProductById(productId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(new ProductResponse(productId, "Product 1", "Description 1", 10.0), response.getBody());
    }

    @Test
    public void getProductById_NonExistingId_ReturnsBadRequest() {
        Long productId = 1L;
        when(productRepository.findById(productId)).thenReturn(Optional.empty());

        ResponseEntity<Object> response = productService.getProductById(productId);
        ErrorProductResponse errorProductResponse = new ErrorProductResponse(new ProductException("Product not found"));

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(errorProductResponse.getErrorMap(), response.getBody());
    }

    @Test
    public void createProduct_ValidProduct_ReturnsProductResponse() {
        ProductRequest productRequest = new ProductRequest("Product 1", "Description 1", 10.0);
        Product savedProduct = new Product(1L, "Product 1", "Description 1", 10.0);
        when(productRepository.save(any(Product.class))).thenReturn(savedProduct);

        ResponseEntity<Object> response = productService.createProduct(productRequest);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(new ProductResponse(1L, "Product 1", "Description 1", 10.0), response.getBody());
    }

    @Test
    public void createProduct_SaveProductException_ThrowsProductException() {
        ProductRequest productRequest = new ProductRequest("Product 1", "Description 1", 10.0);
        when(productRepository.save(any(Product.class))).thenThrow(new RuntimeException("Error saving product"));

        try {
            productService.createProduct(productRequest);
        } catch (ProductException e) {
            assertEquals("Error save product", e.getMessage());
        }
    }
    @Test
    public void updateProduct_ValidIdAndProduct_ReturnsProductResponse() {
        Long productId = 1L;
        ProductRequest productRequest = new ProductRequest("Updated Product", "Updated Description", 20.0);
        Product existingProduct = new Product(productId, "Product 1", "Description 1", 10.0);
        when(productRepository.findById(productId)).thenReturn(Optional.of(existingProduct));
        when(productRepository.save(any(Product.class))).thenReturn(existingProduct);

        ResponseEntity<Object> response = productService.updateProduct(productId, productRequest);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(new ProductResponse(productId, "Updated Product", "Updated Description", 20.0), response.getBody());
    }

    @Test
    public void updateProduct_ProductNotFoundException_ReturnsBadRequest() {
        Long productId = 1L;
        ProductRequest productRequest = new ProductRequest("Updated Product", "Updated Description", 20.0);
        when(productRepository.findById(productId)).thenReturn(Optional.empty());

        ResponseEntity<Object> response = productService.updateProduct(productId, productRequest);
        ErrorProductResponse errorProductResponse = new ErrorProductResponse(new ProductException("Product not found"));
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(errorProductResponse.getErrorMap(), response.getBody());
    }
    @Test
    public void deleteProduct_ExistingId_ReturnsDeleteProductResponse() {
        Long productId = 1L;
        Product existingProduct = new Product(productId, "Product 1", "Description 1", 10.0);
        when(productRepository.findById(productId)).thenReturn(Optional.of(existingProduct));

        ResponseEntity<Object> response = productService.deleteProduct(productId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(new DeleteProductResponse(productId, "Product 1", "product delete"), response.getBody());
        verify(productRepository, times(1)).delete(existingProduct);
    }

    @Test
    public void deleteProduct_NonExistingId_ReturnsBadRequest() {
        Long productId = 1L;
        when(productRepository.findById(productId)).thenReturn(Optional.empty());

        ResponseEntity<Object> response = productService.deleteProduct(productId);
        ErrorProductResponse errorProductResponse = new ErrorProductResponse(new ProductException("Product not found"));

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(errorProductResponse.getErrorMap(), response.getBody());
        verify(productRepository, never()).delete(any(Product.class));
    }
}