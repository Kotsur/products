package com.example.product.web;

import com.example.product.entity.Product;
import com.example.product.payload.request.ProductRequest;
import com.example.product.service.ProductService;
import com.example.product.validations.ResponseErrorValidation;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.validation.BindingResult;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@WebMvcTest(ProductController.class)
class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    @MockBean
    private ResponseErrorValidation responseErrorValidation;

    @Test
    public void getAllProducts_ReturnsListOfProducts() throws Exception {
        Product product1 = new Product(1L, "Product 1", "Description 1", 10.0);
        Product product2 = new Product(2L, "Product 2", "Description 2", 20.0);
        List<Product> productList = Arrays.asList(product1, product2);
        when(productService.getAllProducts()).thenReturn(ResponseEntity.ok(productList));

        mockMvc.perform(MockMvcRequestBuilders.get("/products"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("[{\"id\": 1, \"name\": \"Product 1\", \"description\": \"Description 1\", \"price\": 10.0}, {\"id\": 2, \"name\": \"Product 2\", \"description\": \"Description 2\", \"price\": 20.0}]"))
                .andDo(print());
    }

    @Test
    public void getProductById_ReturnsProduct() throws Exception {
        Product product = new Product(1L, "Product 1", "Description 1", 10.0);
        when(productService.getProductById(1L)).thenReturn(ResponseEntity.ok(product));

        mockMvc.perform(MockMvcRequestBuilders.get("/products/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Product 1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value("Description 1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price").value(10.0))
                .andDo(print());
    }

    @Test
    public void getProductById_ReturnsNotFound() throws Exception {
        when(productService.getProductById(anyLong())).thenReturn(ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found"));

        mockMvc.perform(MockMvcRequestBuilders.get("/products/1"))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().string("Product not found"))
                .andDo(print());
    }

    @Test
    public void createProducts_WithInvalidInput_ReturnsBadRequest() {
        ProductController productController = new ProductController(productService, responseErrorValidation);
        BindingResult bindingResult = mock(BindingResult.class);
        ResponseEntity<Object> errorResponseEntity = ResponseEntity.badRequest().body("Invalid input");

        when(responseErrorValidation.mapValidationService(bindingResult)).thenReturn(errorResponseEntity);

        ResponseEntity<Object> responseEntity = productController.createProducts(null, bindingResult);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(errorResponseEntity, responseEntity);

        verify(productService, never()).createProduct(any());
    }

    @Test
    public void createProducts_WithValidInput_ReturnsCreated() {
        ProductController productController = new ProductController(productService, responseErrorValidation);
        BindingResult bindingResult = mock(BindingResult.class);
        ProductRequest productRequest = new ProductRequest("Test Product", "Test Description", 10.0);
        ResponseEntity<Object> successResponseEntity = ResponseEntity.ok("Product created");

        when(responseErrorValidation.mapValidationService(bindingResult)).thenReturn(null);

        when(productService.createProduct(productRequest)).thenReturn(successResponseEntity);

        ResponseEntity<Object> responseEntity = productController.createProducts(productRequest, bindingResult);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(successResponseEntity, responseEntity);

        verify(productService, times(1)).createProduct(productRequest);
    }
    @Test
    public void updateProducts_WithInvalidInput_ReturnsBadRequest() {
        ProductController productController = new ProductController(productService, responseErrorValidation);
        BindingResult bindingResult = mock(BindingResult.class);
        ResponseEntity<Object> errorResponseEntity = ResponseEntity.badRequest().body("Invalid input");

        when(responseErrorValidation.mapValidationService(bindingResult)).thenReturn(errorResponseEntity);

        ResponseEntity<Object> responseEntity = productController.updateProducts(1L, null, bindingResult);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(errorResponseEntity, responseEntity);

        verify(productService, never()).updateProduct(anyLong(), any());
    }

    @Test
    public void updateProducts_WithValidInput_ReturnsOk() {
        ProductController productController = new ProductController(productService, responseErrorValidation);
        BindingResult bindingResult = mock(BindingResult.class);
        ProductRequest productRequest = new ProductRequest("Updated Product", "Updated Description", 20.0);
        ResponseEntity<Object> successResponseEntity = ResponseEntity.ok("Product updated");

        when(responseErrorValidation.mapValidationService(bindingResult)).thenReturn(null);

        when(productService.updateProduct(1L, productRequest)).thenReturn(successResponseEntity);

        ResponseEntity<Object> responseEntity = productController.updateProducts(1L, productRequest, bindingResult);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(successResponseEntity, responseEntity);

        verify(productService, times(1)).updateProduct(1L, productRequest);
    }
}

