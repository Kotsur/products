create table product
(
    id          bigint auto_increment
        primary key,
    description varchar(255) null,
    name        varchar(255) null,
    price       double       null
);
