# Product Management Application

This is a sample application for managing products.
Getting Started
Prerequisites

    Java Development Kit (JDK) 8 or above
    Maven
    Docker

### Clone the Repository
`git clone https://gitlab.com/Kotsur/products.git`
### Database Setup
To set up the database for the Product Management Application without using Docker, follow these steps:
1. Install and configure MySQL on your machine.
2. Open a command-line interface and log in to MySQL using the appropriate credentials:
   `mysql -u <username> -p`
3. Create a new database named product:
   `CREATE DATABASE product;`

4. Create a new user and grant necessary privileges to the user:


    CREATE USER 'user'@'localhost' IDENTIFIED BY 'password';

    GRANT ALL PRIVILEGES ON product.* TO 'user'@'localhost';

    FLUSH PRIVILEGES;


Replace user with your desired username and password with your desired password.

5. Update the database connection properties in the application.properties file located in the src/main/resources directory of the project:


    spring.datasource.url=jdbc:mysql://localhost:3306/product?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC&useUnicode=yes&characterEncoding=UTF-8
    spring.datasource.username=user
    spring.datasource.password=password
Adjust the URL, username, and password based on your MySQL configuration.

6. Build and run the application as described in the previous sections.

Note: Make sure the MySQL service is running before starting the application.

### Running the Application without Docker
1. Navigate to the project directory.
2. Build the project and create the executable JAR file:
   `mvn clean package`

3. Run the application:
   `java -jar target/products.jar`

*The application will start and be accessible at http://localhost:8080.*

### Running the Application with Docker
1. Make sure you have Docker and Docker Compose installed.
2. Navigate to the project directory.
3. Build the Docker image and start the application container:
   `docker-compose up -d --build`

*The application will start and be accessible at http://localhost:8080.*

# Configuration

The application can be configured by modifying the **application.properties** file located in the **src/main/resources** directory.

### Database Configuration
By default, the application uses a MySQL database running in a Docker container. If you want to use a different MySQL server, update the following properties in the **application.properties** file:


    spring.datasource.url=jdbc:mysql://hostname:port/database
    spring.datasource.username=username
    spring.datasource.password=password
    spring.jpa.hibernate.ddl-auto=update
    spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL8Dialect


Replace **hostname**, **port**, **database**, **username**, and **password** with your database connection details.